<div>
    <h1>404 NOT FOUND MASTER</h1>
</div>

# Acerca de mí
¡Hola! 

Soy Andree, estoy creando mi portafolio en [devChallenges](https://devchallenges.io/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/404-not-found-master-test), actualmente soy Desarrollador Front End, y me agrada seguir aprendiento.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este es un proyecto de 'DevChallenge - 404 Not Found', cada commit representa una avance en HTML, CSS.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://andreemalerva.gitlab.io/404-not-found-master-test/)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA
